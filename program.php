<!DOCTYPE html>
    <html lang="en">
        <head>
            <title>Program Rumah Sakit</title>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" href="profil.css">
        </head>
        <body>
            <header>
                <div class="logo">Portofolio.</div>
                <nav>
                        <div class="menu">
                            <ul>
                                <li><a href='Profil.html'>Home</a></li>
                                <li><a href='program.php'>Program</a></li>
                            </ul>
                        </div>
                </nav>
            </header>
            <main>
                <article>
                    <div class="countainer">
                        <h3 class="data">Data pasien.</h3>
                        <a class="tombol" href="input.php">+ Tambah Data Baru</a>
                        <table class="pasien">
                            <tr>
                                <th>No.</th>
                                <th>Nama</th>
                                <th>Gender</th>
                                <th>Usia</th>
                                <th>Telephone</th>
                                <th>Alamat</th>
                                <th>Dokter</th>
                                <th>Opsi</th>
                            </tr>
                            <?php
                            include "koneksi.php";
                            $no =1;
                            $query_mysqli = mysqli_query($host, "SELECT * FROM user");
                            if(mysqli_num_rows($query_mysqli) > 0){
                            while($hasil = mysqli_fetch_array($query_mysqli)){
                            ?>
                            <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $hasil['Nama'];?></td>
                                <td><?php echo $hasil['Gender'];?></td>
                                <td><?php echo $hasil['Usia'];?></td>
                                <td><?php echo $hasil['Telephone'];?></td>
                                <td><?php echo $hasil['Alamat'];?></td>
                                <td><?php echo $hasil['Dokter'];?></td>
                                <td>
                                    <a class="hapus" href="delete.php?Nama=<?php echo $hasil['Nama'] ?>">Hapus</a>
                                </td>
                            </tr>
                            <?php }} else { ?>
                            <tr>
                                <td colspan="8" align="center">-    Data Kosong     -</td>
                            </tr>
                            <?php } ?>
                        </table>
                    </div>
                </article>
                <aside>
                    <div class="back">
                    
                    <form class="array1" method="POST">
                    <table class="array" >
                    <tr>
                        <td><label>Data Dokter Rumah Sakit</label></td>
                    </tr>
                    <tr>
                        <td><input type="submit" name="cek" value="Cek"></td>
                    </tr>
                    <tr>
                        <td>
                            <?php
                                if(isset($_POST['cek'])){
                                    $arrdata=array("Dokter Umum", "Dokter THT","Dokter Gigi","Dokter Syaraf", "Dokter Anak");
                                    echo "<br>Menampilkan Data Dokter : <br><br>";
                                    foreach($arrdata as $data){
                                        echo "- " .$data. "<br>";
                                    }
                                }
                            ?>
                        </td>
                    </tr>
                    </table>
                    </form>
                    </div>
                </aside>
            </main>
            <footer>
                <small>Copyright &copy; 2021 Portofolio. All Right Reserved by Rosa Agnelia Rahmawati</small>
            </footer>
        </body>
    </html>